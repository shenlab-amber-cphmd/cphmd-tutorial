* Replica stream REP
*

set ph = REPPH
set temp = REPTEMP

PHMD RESPH OLDPH 7.0 NEWPH @ph PKATEMP 300.0

! Nobonded option
! ---------------
nbond elec atom fswitch cdie vdw vatom vswitch -         ! NONBOND SETUP FOR PME
      ctonnb @nbcuton ctofnb @nbcutoff cutnb @nbcut cutim @imcut -
      inbfrq -1 imgfrq -1 -
      ewald pmew fftx @fftx ffty @ffty fftz @fftz  kappa .34 spline order 6
!energy



open read unit 11 card name stage@lstage/@in_ph@ph_temp@temp.rst
open write unit 12 form name stage@stage/@in_ph@ph_temp@temp.rst
open write unit 13 unform name stage@stage/@in_ph@ph_temp@temp.dcd
open write unit 14 form name stage@stage/@in_ph@ph_temp@temp.ene
open write unit 25 form name stage@stage/@in_ph@ph_temp@temp.lamb

! Run Dynamics
! --------------
DYNA LEAP CPT RESTART time 0.002 nstep @ntot ISEED S1 S2 S3 S4 -
     nprint @exfrq iprfrq 1000 ntrfrq @exfrq -
     iunread 11 iunwri 12 iuncrd 13 iunvel -1 kunit 14 -
     nsavc 500 nsavv 0 isvfrq @ntot -
     echeck 100 - 
     firstt @temp finalt @temp tstruc @temp -
     twindl -10.0 twindh 10.0 -
     pconstant pmass @pmass pref 1.0 pgamma 20.0 tbath @temp -
     hoover reft @temp tmass @tmass

stop

