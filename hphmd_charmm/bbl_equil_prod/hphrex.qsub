#!/usr/bin/tcsh
#$ -S /bin/tcsh
#$ -cwd
#$ -V
#$ -N BBL_tuto
#$ -o $JOB_ID.o
#$ -e $JOB_ID.e
#$ -j y
#$ -l h_data=500M,h_rt=996:00:00
#$ -pe ppn64 64
##$ -pe mpirun1 32
#$ -R y

#Production should be run with 256
### Prepare environment. Do not change.
# Due to a bug in SGE qrsh will dump core if it exits immediately.
limit coredumpsize 4
limit cputime      unlimited
limit filesize     unlimited
limit datasize     unlimited
limit stacksize    unlimited
limit memoryuse    unlimited
limit vmemoryuse   unlimited

# Change this for each job
set pdbname = bbl  
set temp    = 298.0
set conc    = 0.15
set crysph  = 7.4  # Crystal pH 
set struct  = ${pdbname}

# This is environment dependent 
set rundir = /home/$USER/simulations/bbl_tutorial/run
set workdir = `pwd`

# Update this for restarts
set mystage = 2     #equilibration, >0: production run

# Don't need to change this
set lstage = `echo $mystage 1 | awk '{print $1-$2}'`
set nrep = 0 # counter for all reps

if ( $mystage == 0 ) then # For Equilibration
  set input = ${pdbname}_equilibration
  mkdir $rundir
  cp ${struct}.* $rundir/.
  sed "s/PDBNAME/$pdbname/g;s/TEMPERATURE/$temp/g;s/CONCENTRATION/$conc/g;s/CRYSPH/$crysph/g" equilibration_hphmd.inp > $rundir/$input.inp

else # For Production

  set input = ${pdbname}_ephrex
  # for each replica set pH, and four unique random number seeds
  foreach ph ( 3.0 3.5 4.0 4.5 5.0 5.5 6.0 6.5 )

      sed "s/REPPH/$ph/g;s/REPTEMP/$temp/g;s/JOB/$pdbname/g;s/REP/$nrep/g;s/CRYSPH/$crysph/g" rep.cmd > $rundir/rep.cmd_$nrep
     
      if ( $mystage == 1 ) then
         cp $rundir/stage0/${pdbname}_equil_4.rst $rundir/stage0/${pdbname}_ph${ph}.rst_${nrep}
      endif

      @ nrep++
  end

  sed "s/PDBNAME/$pdbname/g;s/TEMPERATURE/$temp/g;s/CONCENTRATION/$conc/g;s/STAGE/$mystage/g;s/NREP/$nrep/g;s/CRYSPH/$crysph/g" production_hphrex.inp > $rundir/$input.inp

endif

cd $rundir
mkdir stage$mystage

### Job-specific parameter. Re-evaluate for every job.
set cmdline = "/state/partition1/opt/charmm/parallel/charmm-c42a2_ephmd_pme_dph_volt"

### Implementation-specific parameter. Set this only once. OpenMPI
### doesn't need a machine file, but requires --leave-session-attached
set mpirun = "mpirun --leave-session-attached"

### Submit job using mpirun. Do not change.
$mpirun -np $NSLOTS $cmdline < $input.inp >& $input.out

### Clean up scratch
 
exit
