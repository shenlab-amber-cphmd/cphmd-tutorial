* Replica stream REP
*

set ph = REPPH
PHMD RESPH OLDPH @crysph NEWPH @ph PKATEMP @temp

! Nobonded option
! ---------------
nbond elec atom switch cdie vdw vatom vswitch -
     ctonnb @nbcuton ctofnb @nbcutoff cutnb @nbcut cutim @imcut -
     inbfrq -1 imgfrq -1 -
     ewald pmew fftx 60 ffty 60 fftz 60  kappa .34 spline order 6
! fftx/y/z should be set to box length and has prime factors of 2, 3, and 5
! fftx/y/z should be set to values as close to A/B/C in the pbc file so the grid spacing is close to 1.

! File to Restart From
open read  unit 11 card   name stage@lstage/@in_ph@ph.rst
! Files to make per run
open write unit 12 form   name stage@stage/@in_ph@ph.rst
open write unit 13 unform name stage@stage/@in_ph@ph.dcd
open write unit 14 form   name stage@stage/@in_ph@ph.ene
open write unit 25 form   name stage@stage/@in_ph@ph.lamb

! Run Dynamics
! --------------
DYNA LEAP CPT RESTART time 0.002 nstep @ntot -
     nprint @exfrq iprfrq 50000 ntrfrq @exfrq -
     iunread 11 iunwri 12 iuncrd 13 iunvel -1 kunit 14 -
     nsavc @exfrq nsavv 0 isvfrq 50000 -
     echeck 100 - 
     firstt @temp finalt @temp tstruc @temp -
     twindl -10.0 twindh 10.0 -
     pconstant pmass @pmass pref 1.0 pgamma 20.0 tbath @temp -
     hoover reft @temp tmass @tmass
stop

