* Equilibration 6.2, fixed-charge
* Adapted from CHARMM-GUI script
* Usage: charmm < step6.2_equi.inp > step6.2_equi.out
*

! Change dimension
! ----------------
DIMENS CHSIZE 2000000

! Set variables
! -------------
! In general only settings below need to be modified
! -------------
set toppar   = toppar_phmd_c27.str          ! top & par files

set stage    = 2                            ! stage number
calc lstage  = @stage - 1                   ! previous stage

set segi     = 3wkv                         ! segment ID of protein
set in       = step5_assembly               ! input *.psf/crd/str
set img      = crystal_image.str            ! crystal symmetry operation *.str
set time     = 0.05                         ! MD length, ns
set timestep = 0.002                        ! step length, ps
set temp     = 300                          ! temperature, K
set freq     = 500                          ! frequency
calc nstep   = ( @time * 1000 ) / @timestep ! total MD steps

! Nonbond options
! ---------------
! Recommended settings for C36 lipid, better not change
! ---------------
set ctonnb =  8.0 ! the radius at which switch function begins to take effect
set ctofnb = 12.0 ! cut-off radius beyond which interactions are ignored
set cutnb  = 14.0 ! cut-off radius for non-bonded list
set cutim  = 16.0 ! cut-off radius for image list

! PME options
! -----------
! Grid spacing in each direction should be approximately 1 Angstromg (0.8-1.2)
! For grid numbers, only powers of 2, 3 and 5 are allowed. More powers of 2, the better
! -----------
set fftx = 80
set ffty = 80
set fftz = 96

! Read topology and parameter files
! ---------------------------------
stream @toppar

! Read system structure
! ---------------------
open unit 1 read card name @in.psf
read psf  card unit 1
close unit 1

open unit 1 read card name @in.crd
read coor card unit 1
close unit 1

! Read system information
! -----------------------
stream @in.str

! Define selections
! -----------------
define PROT sele segi @segi end
define HV   sele PROT .and. ( .not. hydrogen ) end
define BB   sele ( type C .or. type N  .or. type CA ) .and. PROT end
define TERM sele PROT .and. ( type CAY .or. type CY  .or. type OY  .or. -
                              type HY1 .or. type HY2 .or. type HY3 .or. -
                              type NT  .or. type HT1 .or. type HT2 .or. -
                              type HNT .or. type CAT .or. type HT3 ) end
define SC   sele HV .and. .not. ( BB .or. type O .or. TERM ) end
define PWAT sele segi PWAT .and. .not. hydrogen end
define BWAT sele resn TIP3 .and. .not. PWAT end
define LIP  sele segi MEMB end
define ION  sele segi @posid .or. segi @negid end
define SOLU sele PROT end
define SOLV sele .not. SOLU end

! Turn on faster options
! ----------------------
faster on
shake bonh param

! Image Setup
! -----------
open unit 1 read card name @img
CRYSTAL DEFINE @XTLtype @A @B @C @alpha @beta @gamma
CRYSTAL READ UNIT 1 CARD

! Image centering
! ---------------
IMAGE BYSEGID XCEN 0.0 YCEN 0.0 ZCEN @zcen sele solu end
IMAGE BYRESID XCEN 0.0 YCEN 0.0 ZCEN @zcen sele solv end

! Setup Restraints for Protein and Lipids
! ---------------------------------------
! Equilibration Scheme ( 6 cycles )
!------------------------------------------------------------------------
!        1 cycle    2 cycle    3 cycle    4 cycle    5 cycle    6 cycle
! leng(ps)   50         50        100        100        100        100
! step(fs)    1          2          2          2          2          2
! Dyna     LANG       LANG        CPT        CPT        CPT        CPT
!------------------------------------------------------------------------
! BB       10.0        5.0        5.0        2.5        1.5        1.0  
! SC+PWAT   5.0        5.0        5.0        2.5        1.5        1.0
! wforce    2.5        2.5        1.0        0.5        0.1        0.0
! tforce    2.5        2.5        1.0        0.5        0.1        0.0
! mforce    2.5        2.5        1.0        0.5        0.1        0.0
! BWAT+ION  0.0        0.0        0.0        0.0        0.0        0.0
! fcis/fc2  250        100         50         50         25          0
!------------------------------------------------------------------------

cons harm force 5.0 sele BB end
cons harm force 5.0 sele SC .or. PWAT end

set wforce = 2.5 ! force constant to keep the water molecules away from the hydrophobic core
set tforce = 2.5 ! force constant to keep the lipid tail below +/- %
set mforce = 2.5 ! force constant to keep the lipid head groups close to target values
stream membrane_lipid_restraint.str

set fcis   = 100 ! dihedral restraint force constant to keep the cis double bonds
set fc2    = 100 ! dihedral restraint force constant to keep the c2  chirality
stream membrane_lipid_restraint2.str

! Dynamics
! ---------------------------------------------------------------------------
! To reduce the possible problem with the numerical integration with
! the uncorrelated system, 1 fs time-step is used only for the first-step
! of equilibration. It is still possible that you may need to use 1 fs for
! the next equilibration steps if your system is initially very very unstable
! (rare cases).
! ---------------------------------------------------------------------------

open read  unit 11 form   name stage6.@lstage/step6.@{lstage}_equi.rst
open write unit 12 form   name stage6.@stage/step6.@{stage}_equi.rst
open write unit 13 unform name stage6.@stage/step6.@{stage}_equi.dcd
open write unit 14 form   name stage6.@stage/step6.@{stage}_equi.ene

scalar fbeta set 5.0 sele .not. type H* end ! Langevin friction coefficient

dyna langevin leap restart -
     nstep @nstep time @timestep -
     iunrea 11 iunwri 12 iuncrd 13 kunit 14 iunvel -1 -
     nprint @freq iprfrq @freq ntrfrq @freq -
     nsavc @freq nsavv 0 isvfrq @freq -
     iasors 0 iasvel 1 iscvel 0 ichecw 1 -
     firstt @temp finalt @temp tstruc @temp -
     twindl 0.0 twindh 0.0 -
     echeck 20.0 -
     rbuf 0.0 tbath @temp -
     elec atom cdie vdw vatom vfswitch bycb -
     ctonnb @ctonnb ctofnb @ctofnb cutnb @cutnb cutim @cutim -
     ewald pmew fftx @fftx ffty @ffty fftz @fftz kappa .34 spline order 6 -
     inbfrq -1 imgfrq -1

cons harm clear

! Write outputs
! -------------
open unit 90 write card name stage6.@stage/step6.@{stage}_equi.pdb
write coor pdb unit 90
* 6.@stage PDB
*

stop
