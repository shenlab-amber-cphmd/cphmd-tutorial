* Replica stream REP, including production run
*

! Set variables
! -------------
set ph = REPPH

! Reset pH conditions for PHMD of each replica
! --------------------------------------------
PHMD RESPH OLDPH @heph NEWPH @ph PKATEMP @temp

! Run dynamics
! ------------
open unit 11 read  card   name stage@lstage/@in.ph@ph.rst
open unit 12 write card   name stage@stage/@in.ph@ph.rst
open unit 13 write unform name stage@stage/@in.ph@ph.dcd
open unit 14 write form   name stage@stage/@in.ph@ph.ene
open unit 25 write form   name stage@stage/@in.ph@ph.lamb

dyna cpt leap restart -
     nstep @nstep time @timestep -
     iunrea 11 iunwri 12 iuncrd 13 kunit 14 iunvel -1 -
     nprint @freq iprfrq 50000 ntrfrq @freq -
     nsavc @freq nsavv 0 isvfrq 50000 -
     iasors 0 iasvel 1 iscvel 0 ichecw 1 -
     firstt @temp finalt @temp tstruc @temp -
     twindl -10.0 twindh 10.0 -
     echeck 20.0 -
     hoover reft @temp tmass @tmass -
     pconst pint tbath @temp pref 1.0 pgamma 20.0 pmass @pmass -
     elec atom cdie vdw vatom vfswitch bycb -
     ctonnb @ctonnb ctofnb @ctofnb cutnb @cutnb cutim @cutim -
     ewald pmew fftx @fftx ffty @ffty fftz @fftz kappa .34 spline order 6 -
     inbfrq -1 imgfrq -1

stop
