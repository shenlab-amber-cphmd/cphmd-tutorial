* States file for Hybrid-PHMD
* Syntax:
* RESNAME MODEL_PKA PARA PARB
*  ATOM_TYPE CHARGE(1) CHARGE(2) [RAD(1) RAD(2)]
*
! ver. 2.0 ; April 1, 2010
! #######################################################################
! These model parameters were redone using CPT-PME-PBC simulations at 300K
! using top/par_all22_prot_cmap_phmd
! (LYS,ASP,GLU,HSP)
! By Jason Wallace

! generation 2 ; June, 2011
! #######################################################################
! v2.0 model parameters trained to reproduce model compound pKa's
! by successive iterations of pKa calculation and parameter adjustment.
! pKa's were calculated using same conditions for v2.0 
! combined with pH-based replica exchange protocol and using a 
! lambda update frequency of 1 / 10 dynamic steps.
! Updating lambda at an interval of 10 gives more stable 
! trajectories with more realistic water structure around titrating sites.
! Calculated pKa given is average of 10 trials from last iteration 
! and error is 1 standard deviation.

! Parameterize TYR; February, 2013
! #######################################################################
! By Nandun Thellamurege

! Reparameterize HSP; May, 2014
! #######################################################################
! Wei Chen found a bug in in phmd.src, which switches the delta with the
! epsilon site when calculating the protonation force on HSP. The bug
! does not affect the macroscopic pKa of His but makes the delta site’s
! pKa slightly higher than the epsilon site in the model compound.
! By Zhi Yue

! Parameterize Arg; July, 2014
! #######################################################################
! By Zhi Yue

! Revise for distributed C22+CMPA FF; March, 2015
! #######################################################################
! By Zhi Yue

! pKa(calc) = 9.53
! ----------------
TYR   9.6  -79.5145 -0.00947038 2.0
CZ    0.11  -0.29
OH   -0.54  -0.71
HH    0.43   0.00 1.0 0.0

! pKa(calc) = 10.36 \pm 0.06
! --------------------------
LYS   10.4 -75.8231 0.769208 2.0
CE    0.21   0.40
HE1   0.05  -0.05
HE2   0.05  -0.05
NZ   -0.30  -0.98
HZ1   0.33   0.34
HZ2   0.33   0.34
HZ3   0.33   0.00 1.0 0.0

! pKa(calc) = 12.60 \pm 0.02
! --------------------------
ARG 12.5 -64.8787 1.54903 2.0
CD    0.20   0.00
HD1   0.09   0.09
HD2   0.09   0.09
NE   -0.70  -0.70
HE    0.44   0.43
CZ    0.64   0.40
NH1  -0.80  -0.62
HH11  0.46   0.31
HH12  0.46   0.00 1.0 0.0
NH2  -0.80  -0.62
HH21  0.46   0.31
HH22  0.46   0.31

! pKa(calc) = 4.04
! ----------------
ASP1  4.0 -82.676 0.265962 2.0
CB   -0.21  -0.28
HB1   0.09   0.09
HB2   0.09   0.09
CG    0.75   0.62
OD1  -0.61  -0.76
OD2  -0.55  -0.76
HD1   0.44   0.00 1.0 0.0
HD2   0.00   0.00

ASP2  4.0 -82.2749 0.2648 2.0 -31.5967 0.500523
          -28.6888 60.4312 -31.6319 0.5 -82.5611 0.264153370750874
CB   -0.21  -0.28
HB1   0.09   0.09
HB2   0.09   0.09
CG    0.75   0.62
OD1  -0.55  -0.76
OD2  -0.61  -0.76
HD1   0.00   0.00
HD2   0.44   0.00 1.0 0.0

! pKa(calc) = 4.38 \pm 0.05
! -------------------------
GLU1  4.4 -82.9575 0.248635 2.0
CG   -0.21  -0.28
HG1   0.09   0.09
HG2   0.09   0.09
CD    0.75   0.62
OE1  -0.61  -0.76
OE2  -0.55  -0.76
HE1   0.44   0.00 1.0 0.0
HE2   0.00   0.00

GLU2  4.4 -82.9575 0.248635 2.0 -31.9101 0.505489 2.5
          -28.7907 60.9986 -31.9878 0.50000 -83.98099 0.249025
CG   -0.21  -0.28
HG1   0.09   0.09
HG2   0.09   0.09
CD    0.75   0.62
OE1  -0.55  -0.76
OE2  -0.61  -0.76
HE1   0.00   0.00
HE2   0.44   0.00 1.0 0.0

! pKa(calc)
! HSD = 6.94 \pm 0.07
! HSE = 7.32 \pm 0.10
! HSD & HSE = 6.77 \pm 0.07
! -------------------------
HSD   6.60  -50.5334 0.534855 2.0
ND1  -0.51  -0.70
HD1   0.44   0.00 1.0 0.0
CE1   0.32   0.25
HE1   0.18   0.13
NE2  -0.51  -0.36
HE2   0.44   0.32
CD2   0.19  -0.05
HD2   0.13   0.09
CG    0.19   0.22
CB   -0.05  -0.08

HSE   7.00  -51.1696 0.684475 2.0 -40.5541 0.310779
            0.0 0.0 0.0 0.0 0.0 0.0
ND1  -0.51  -0.36
HD1   0.44   0.32
CE1   0.32   0.25
HE1   0.18   0.13
NE2  -0.51  -0.70
HE2   0.44   0.00 1.0 0.0
CD2   0.19   0.22
HD2   0.13   0.10
CG    0.19  -0.05
CB   -0.05  -0.09

! Parameterized using penta-alanine peptide using 
! experimental salt (100mM) concentration 
! and neutral blocked opposite terminus
! (see Thurkill_ProteinSci_2006_v15_p1214)
! J. Wallace (May 2010) 

CTALA1  3.7  -90.986 0.186899 2.0
C     0.72   0.34
OT1  -0.61  -0.67
OT2  -0.55  -0.67
HC1   0.44   0.00 1.0 0.0
HC2   0.00   0.00

CTALA2  3.7 -90.986 0.186899 2.0 -30.3771 0.507353 2.5
            -27.0761 57.8287 -30.4887 0.501968 -90.9957 0.187439
C     0.72   0.34
OT1  -0.55  -0.67
OT2  -0.61  -0.67
HC1   0.00   0.00
HC2   0.44   0.00 1.0 0.0

CTASP1  3.7  -90.986 0.186899 2.0
C     0.72   0.34
OT1  -0.61  -0.67
OT2  -0.55  -0.67
HC1   0.44   0.00 1.0 0.0
HC2   0.00   0.00

CTASP2  3.7 -90.986 0.186899 2.0 -30.3771 0.507353 2.5
            -27.0761 57.8287 -30.4887 0.501968 -90.9957 0.187439
C     0.72   0.34
OT1  -0.55  -0.67
OT2  -0.61  -0.67
HC1   0.00   0.00
HC2   0.44   0.00 1.0 0.0

! copied from GB/CTVAL

CTVAL1  3.8 -90.0698 0.231892 2.0
C     0.72   0.34
OT1  -0.61  -0.67
OT2  -0.55  -0.67
HC1   0.44   0.00 1.0 0.0
HC2   0.00   0.00

CTVAL2  3.8 -89.9598 0.231154 2.0 -30.5668 0.497767 2.5
            -28.4794 59.2955 -30.6616 0.499372 -89.8957 0.231252
C     0.72   0.34
OT1  -0.55  -0.67
OT2  -0.61  -0.67
HC1   0.00   0.00
HC2   0.44   0.00 1.0 0.0

CTPHE1  3.3  -90.986 0.186899 2.0
C     0.72   0.34
OT1  -0.61  -0.67
OT2  -0.55  -0.67
HC1   0.44   0.00 1.0 0.0
HC2   0.00   0.00

CTPHE2  3.3 -90.986 0.186899 2.0 -30.3771 0.507353 2.5
            -27.0761 57.8287 -30.4887 0.501968 -90.9957 0.187439
C     0.72   0.34
OT1  -0.55  -0.67
OT2  -0.61  -0.67
HC1   0.00   0.00
HC2   0.44   0.00 1.0 0.0

END
