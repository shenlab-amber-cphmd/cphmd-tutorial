# Scripts and examples for running CpHMD parameterization

Python and Bash scripts to run thermodynamic integration (TI) simulations required for CpHMD parameterization

1. Use `./min.scr NAME` to minimize prepared penta-peptide structures where NAME is the name (without extension) of the parm7 and rst7 files for the penta-peptide. Examples can be found in the single/carboxyl/his folders.

2. For residue types with a single titratable site (like Cys/Lys), use `./heat_equil_prod_param.sh NAME` once. For residue types with two titratable sites (like Asp/His), use `./heat_equil_prod_param.sh NAME` once and then change the arrays in lines 61&62 to 63&64 for Asp/Glu, or change the arrays in lines 67&68 to 69&70, and run again.

3. After the TI calculations, use `python cphmd_parm_fit.py TYPE` where TYPE can be single/double/his. 'single' refers to residue types with a single titratable site; 'double' refers to residue types with two same titratable sites like Asp and Glu; 'his' refers to Histidine.